import time
import random
import multiprocessing as mp
from models.generator.email import Email
from apscheduler.schedulers.background import BackgroundScheduler
import pandas as pd
import plotly.graph_objects as go


class EmailQueueManager:
    __queue = []
    __priority_queue = []
    __retry_queue = []
    __email_data = []

    def __init__(self):
        # !!! You can't edit this section 11:14
        # This var "self.__queue" is the variable that save the current queue of emails
        self.__queue = self.__generate_emails(
            quantity=10
        )
        file_object = open("count_sent.csv", "w+")
        file_object.write("time,sent\n")
        file_object.close()

        file_object = open("count_new.csv", "w+")
        file_object.write("time,new\n")
        file_object.close()

        file_object = open("count_failed.csv", "w+")
        file_object.write("time,failed\n")
        file_object.close()

        scheduler = BackgroundScheduler()
        scheduler.add_job(self.write_graph_info, 'interval', seconds=0.5)
        scheduler.add_job(self.display_graph, 'interval', seconds=10)
        scheduler.start()
        self.__queue_processor()

    def display_graph(self):
        count_sent_df = pd.read_csv('count_sent.csv')
        count_sent_fig = go.Figure(go.Scatter(x=count_sent_df['time'], y=count_sent_df['sent'],
                                              name='Number of emails executed per 0.5 seconds'))
        count_sent_fig.update_layout(title='Number of emails executed per 0.5 seconds',
                                     plot_bgcolor='rgb(230, 230,230)',
                                     showlegend=True)

        count_sent_fig.show()

        count_new_df = pd.read_csv('count_new.csv')
        count_new_fig = go.Figure(go.Scatter(x=count_new_df['time'], y=count_new_df['new'],
                                             name='Number of new emails per 0.5 seconds'))
        count_new_fig.update_layout(title='Number of new emails per 0.5 seconds',
                                    plot_bgcolor='rgb(230, 230,230)',
                                    showlegend=True)

        count_new_fig.show()

        count_failed_df = pd.read_csv('count_failed.csv')
        count_failed_fig = go.Figure(go.Scatter(x=count_failed_df['time'], y=count_failed_df['failed'],
                                                name='Number of failed emails per 0.5 seconds'))
        count_failed_fig.update_layout(title='Number of failed emails per 0.5 seconds',
                                       plot_bgcolor='rgb(230, 230,230)',
                                       showlegend=True)

        count_failed_fig.show()

    def write_graph_info(self):
        count_failed = 0
        count_sent = 0
        count_new = 0
        for x in self.__email_data:
            if x["status"] == "sent":
                count_sent += 1
                continue
            if x["attempts"] == 0 and x["status"] == "pending":
                count_new += 1
                continue
            if x["attempts"] > 0 and x["status"] == "pending":
                count_failed += 1
                continue

        file_object = open("count_sent.csv", "a")
        file_object.write(" %d, %d \n " % (time.time(), count_sent))
        file_object.close()

        file_object = open("count_new.csv", "a")
        file_object.write(" %d, %d \n " % (time.time(), count_new))
        file_object.close()

        file_object = open("count_failed.csv", "a")
        file_object.write(" %d, %d \n " % (time.time(), count_failed))
        file_object.close()

        self.__email_data.clear()

    def __queue_processor(self):
        while True:
            self.__queue += self.__generate_emails(
                quantity=random.randint(1, 10)
            )

            for idx, email in enumerate(self.__queue):
                if email['attempts'] == 0:
                    self.__email_data.append(email)
                if email["status"] == "sent":
                    del self.__queue[idx]
                    continue
                if email["status"] == "pending" and email["prority"] == 1:
                    self.__priority_queue.append(email)
                    del self.__queue[idx]
                    continue
                if email["status"] == "pending" and email["attempts"] > 0:
                    self.__retry_queue.append(email)
                    del self.__queue[idx]
                    continue

            priority_queue_process_start = time.time()
            priority_queue_process = mp.Pool(8)
            priority_queue_process = map(self.__send_email, self.__priority_queue)
            priority_queue_process_stop = time.time()
            priority_queue_process_total = priority_queue_process_stop - priority_queue_process_start
            for idx, result in enumerate(priority_queue_process):
                self.__email_data.append(result["email"])
                if result["status"] == "sent":
                    file_object = open("process.txt", "a")
                    file_object.write(" %s, %d, %r \n " % (result["email"]["id"], result["email"]["attempts"],
                                           False if result["email"]["prority"] != 1 else True))
                    file_object.close()

                    del self.__priority_queue[idx]
                    continue

            retry_queue_process_start = time.time()
            retry_queue_process = mp.Pool(8)
            retry_queue_process = map(self.__send_email, self.__retry_queue)
            retry_queue_process_stop = time.time()
            retry_queue_process_total = retry_queue_process_stop - retry_queue_process_start
            for idx, result in enumerate(retry_queue_process):
                self.__email_data.append(result["email"])
                if result["status"] == "sent":
                    file_object = open("process.txt", "a")
                    file_object.write(" %s, %d, %r \n " % (result["email"]["id"], result["email"]["attempts"],
                                           False if result["email"]["prority"] != 1 else True))
                    file_object.close()

                    del self.__retry_queue[idx]
                    continue

            pending_queue_process_start = time.time()
            pending_queue_process = mp.Pool(8)
            pending_queue_process = map(self.__send_email, self.__queue)
            pending_queue_process_stop = time.time()
            pending_queue_process_total = pending_queue_process_stop - pending_queue_process_start
            for idx, result in enumerate(pending_queue_process):
                self.__email_data.append(result["email"])
                if result["status"] == "sent":
                    file_object = open("process.txt", "a")
                    file_object.write(" %s, %d, %r \n " % (result["email"]["id"], result["email"]["attempts"],
                                           False if result["email"]["prority"] != 1 else True))
                    file_object.close()

                    del self.__queue[idx]
                    continue

            print(f'\npending emails to send: {len(self.__queue)}')
            print(f'pending emails processing time: {pending_queue_process_total} \n')
            print(f'priority emails to send: {len(self.__priority_queue)}')
            print(f'priority emails processing time: {priority_queue_process_total} \n')
            print(f'retry emails to send: {len(self.__retry_queue)}')
            print(f'retry emails processing time: {retry_queue_process_total} \n')
            print(f'TOTAL processing time: {retry_queue_process_total+priority_queue_process_total+pending_queue_process_total} \n')

            time.sleep(0.5)

    # !!! You can't edit this method
    # This method generate an array with many emails that you should send after.
    def __generate_emails(self, quantity):
        return Email().generate_many(quantity=quantity)

    # !!! You can't edit this method
    # This method is used to send fake email, this method could delay from 0 to 1 second by every send process.
    def __send_email(self, email):
        time.sleep(random.uniform(0, 1))

        error = random.randint(0, 1)

        if error == 1:
            email["attempts"] += 1

        response = {
            "status": "sent" if error == 0 else "error",
            "email": {
                **email,
                **{
                    "status": "pending" if error == 1 else "sent",
                }
            },
        }

        print(
            f'[status:{response["email"]["status"]}] ' +
            f'email:{response["email"]["id"]} ' +
            f'attempts:{response["email"]["attempts"]} '
        )

        return response

